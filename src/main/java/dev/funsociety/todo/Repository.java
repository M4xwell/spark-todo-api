package dev.funsociety.todo;

import com.mysql.cj.jdbc.MysqlDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Repository {

    private static final Logger LOG = LoggerFactory.getLogger(Repository.class);

    private MysqlDataSource dataSource;

    private final String GET_QUERY = "SELECT * FROM todos WHERE id = ?;";
    private final String INSERT_QUERY = "INSERT INTO todos (text) VALUES (?);";
    private final String DELETE_QUERY = "DELETE FROM todos  WHERE id = ?;";
    private final String UPDATE_QUERY = "UPDATE todos SET id = ?, text = ? WHERE id = ?;";

    public Repository() {
        MysqlDataSource ds = new MysqlDataSource();
        ds.setURL("jdbc:mysql://localhost:3306/mysql");
        ds.setUser("mysql");
        ds.setPassword("mysql");

        this.dataSource = ds;
    }

    public int save(final String text) throws Exception {
        PreparedStatement statement = getStmt(INSERT_QUERY);

        statement.setString(1, text);
        statement.executeUpdate();

        ResultSet rs = statement.getGeneratedKeys();
        if (rs.next()) {
            int id = rs.getInt(1);
            LOG.info("Create Todo with id {}", id);
            return id;
        }

        LOG.info("Could not create todo with text {}", text);
        throw new Exception("Could not create todo");
    }

    public Todo findByID(final int id) throws Exception {
        PreparedStatement statement = getStmt(GET_QUERY);

        LOG.info("Find Todo by id {}", id);

        statement.setInt(1, id);
        ResultSet rs = statement.executeQuery();

        if (rs.next()) {
            Todo todo = new Todo();
            todo.setId(rs.getInt("id"));
            todo.setText(rs.getString("text"));
            return todo;
        }

        LOG.info("Could not find todo with id {}", id);
        throw new TodoNotFoundException(id);
    }

    public void deleteByID(final int id) throws Exception {
        // check if the todo exists. An exception will be thrown if not
        findByID(id);

        PreparedStatement statement = getStmt(DELETE_QUERY);

        LOG.info("Delete Todo by id {}", id);

        statement.setInt(1, id);
        statement.executeUpdate();
    }


    public void update(final int id, final String text) throws Exception {
        PreparedStatement statement = getStmt(UPDATE_QUERY);

        LOG.info("Update Todo with id {}", id);

        statement.setInt(1, id);
        statement.setString(2, text);
        statement.setInt(3, id);
        statement.executeUpdate();
    }

    private PreparedStatement getStmt(final String query) throws SQLException {
        Connection connection = this.dataSource.getConnection();
        return connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
    }
}
