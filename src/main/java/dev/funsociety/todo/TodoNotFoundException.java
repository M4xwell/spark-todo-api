package dev.funsociety.todo;

public class TodoNotFoundException extends Exception {

    private int id;

    public TodoNotFoundException(final int id) {
        this.id = id;
    }

    @Override
    public String getMessage() {
        return String.format("Todo with id %d not found", this.id);
    }
}
