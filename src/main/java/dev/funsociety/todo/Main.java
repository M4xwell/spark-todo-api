package dev.funsociety.todo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static spark.Spark.delete;
import static spark.Spark.get;
import static spark.Spark.post;
import static spark.Spark.put;

public class Main {

    private static final Logger LOG = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {
        Repository repository = new Repository();

        LOG.info("Start Todo App");

        post("/todo", Resource.postTodo(repository));
        get("/todo/:id", Resource.getTodo(repository));
        delete("/todo/:id", Resource.deleteTodo(repository));
        put("/todo/:id", Resource.putTodo(repository));
    }
}
