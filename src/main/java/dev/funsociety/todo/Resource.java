package dev.funsociety.todo;

import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Route;

import java.sql.SQLException;

public class Resource {

    private static final Logger LOG = LoggerFactory.getLogger(Resource.class);

    public static Route postTodo(final Repository repository) {
        return (req, res) -> {
            try {
                String body = req.body();
                int id = repository.save(body);

                res.body(String.format("/todo/%d", id));
                res.status(201);

                return res.body();
            } catch (SQLException e) {
                LOG.error("Could not create todo: {}", e.getMessage());
                res.status(400);
                return "";
            } catch (Exception e) {
                LOG.error("Error occurred: {}", e.getMessage());
                res.status(500);
                return "";
            }
        };
    }

    public static Route getTodo(final Repository repository) {
        return (req, res) -> {
            try {
                String reqID = req.params("id");
                int id = Integer.parseInt(reqID);

                Todo todo = repository.findByID(id);

                String payload = new Gson().toJson(todo);
                res.body(payload);

                return res.body();
            } catch (SQLException e) {
                LOG.error("Could not find todo: {}", e.getMessage());
                res.status(400);
                return "";
            } catch (TodoNotFoundException e) {
                LOG.error("Could not find todo: {}", e.getMessage());
                res.status(404);
                return "";
            } catch (Exception e) {
                LOG.error("Error occurred: {}", e.getMessage());
                res.status(500);
                return "";
            }
        };
    }

    public static Route deleteTodo(final Repository repository) {
        return (req, res) -> {
            try {
                String reqID = req.params("id");
                int id = Integer.parseInt(reqID);

                repository.deleteByID(id);

                return "";
            } catch (SQLException e) {
                LOG.error("Could not delete todo: {}", e.getMessage());
                res.status(400);
                return "";
            } catch (TodoNotFoundException e) {
                LOG.error("Could not find todo: {}", e.getMessage());
                res.status(404);
                return "";
            } catch (Exception e) {
                LOG.error("Error occurred: {}", e.getMessage());
                res.status(500);
                return "";
            }
        };
    }

    public static Route putTodo(final Repository repository) {
        return (req, res) -> {
            try {
                String reqID = req.params("id");
                int id = Integer.parseInt(reqID);
                String body = req.body();

                repository.update(id, body);

                return "";
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        };
    }
}
